<?php
if (isset($_GET['bobot'])) {
  $id_bobot = $_GET['bobot'];
  // echo "<script>alert('$id_bobot');</script>";
} else {
  echo "<script>window.location.href='../new/pages/auth/?m=denied';</script>";
}

$sql_bobot0 = mysqli_query($koneksi, "select * from tbl_bobot where id_bobot='$id_bobot'");
$row = mysqli_fetch_array($sql_bobot0, MYSQLI_ASSOC);

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Laporan Id Bobot #<?= $id_bobot; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Ranking</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col text-right">
          <a href="#" class="btn btn-info" onclick="cetak()">
            <h3 class="card-title"><i class="fas fa-print"></i> Cetak</h3>
          </a>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content" style="font-family: 'Times New Roman', Times, serif;">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-12 mb-2">
            <h6>BATIK HATTA</h6>
            <h6>No. Telp : 081333205907</h6>
            <h6>Jalan Graha Mukti Raya No. 361, Tlogosari, Pedurungan, Semarang</h6>
            <h6>e-mail : butikbatikhatta@gmail.com Instagram : butikbatikhatta</h6>
          </div>
          <div class="col-md-12 mb-4 bg-black" style="height: 4px;"></div>
          <div class="col-md-12 mb-4">
            <h4 class="text-center mb-5">Laporan Hasil Kalkulasi</h4>
            <h6>Tanggal kalkulasi: <?php echo $row['created_time']; ?></h6>
            <h6>Tanggal Cetak: <?php echo date("Y/m/d"); ?></h6>
          </div>
          <div class="col-md-12">
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title"> Hasil Bobot Kriteria</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <?php
                $no = 1;
                $sql_bobot = mysqli_query($koneksi, "select * from tbl_bobot where id_bobot='$id_bobot'");
                $rows = mysqli_num_rows($sql_bobot);
                if ($rows > 0) {
                ?>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center align-middle">Hasil Bobot >></th>
                        <th class="text-center align-middle">Bobot 1</th>
                        <th class="text-center align-middle">Bobot 2</th>
                        <th class="text-center align-middle">Bobot 3</th>
                        <th class="text-center align-middle">Bobot 4</th>
                        <th class="text-center align-middle">Bobot 5</th>
                        <!-- <th class="text-center align-middle">Status</th> -->
                        <!-- <th class="text-center align-middle">Status</th>
										 <th class="text-center align-middle">Aksi</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      while ($f_ranking = mysqli_fetch_array($sql_bobot)) {
                        // $no1 = $no++;
                        $bobot1 = $f_ranking['bobot'];
                        $bobot2 = $f_ranking['bobot2'];
                        $bobot3 = $f_ranking['bobot3'];
                        $bobot4 = $f_ranking['bobot4'];
                        $bobot5 = $f_ranking['bobot5'];
                        // $status_bobot = $f_ranking['status_bobot'];
                        // $status_bobot = $f_ranking['status_bobot'];
                      ?>
                        <tr>
                          <td class="text-center align-middle"></td>
                          <td class="text-center align-middle"><?= $bobot1; ?></td>
                          <td class="text-center align-middle"><?= $bobot2; ?></td>
                          <td class="text-center align-middle"><?= $bobot3; ?></td>
                          <td class="text-center align-middle"><?= $bobot4; ?></td>
                          <td class="text-center align-middle"><?= $bobot5; ?></td>
                          <!-- <td class="text-center align-middle"><?= $status_bobot; ?></td> -->
                          </td>
                        </tr>
                    <?php
                      }
                    } else {
                      echo "<center><span class='badge bg-danger'><marquee><h3> Data Masih Kosong , Silahkan Klik Mulai Hitung </h3></marquee></span></center>";
                    }
                    ?>
                    </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-12">
            <div class="card card-dark">
              <div class="card-header">
                <!-- <a href="#" class="btn btn-info" onclick="hitung_ulang()"> -->
                <h3 class="card-title"></i> Hasil Akhir </h3>
                <!-- </a> -->

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <?php
                $no = 1;
                $sql_ranking = mysqli_query($koneksi, "select *from tbl_ranking where id_bobot='$id_bobot' order by nilai_v desc");
                $rows = mysqli_num_rows($sql_ranking);
                if ($rows > 0) {
                ?>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center align-middle">Ranking</th>
                        <th class="text-center align-middle">Nama Alternatif</th>
                        <th class="text-center align-middle">Nilai</th>
                        <!-- <th class="text-center align-middle">Status</th>
											<th class="text-center align-middle">Aksi</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      while ($f_ranking = mysqli_fetch_array($sql_ranking)) {
                        // $no1 = $no++;
                        $nilai_v = $f_ranking['nilai_v'];
                        $nm_alter = $f_ranking['nm_alter'];
                        // $status_bobot = $f_ranking['status_bobot'];
                      ?>
                        <tr>

                          <td class="text-center align-middle"><?= $no++; ?></td>
                          <td class="text-center align-middle"><?= $nm_alter; ?></td>
                          <td class="text-center align-middle"><?= $nilai_v; ?></td>
                          </td>
                        </tr>
                    <?php
                      }
                    } else {
                      echo "<center><span class='badge bg-danger'><marquee><h3> Data Masih Kosong , Silahkan Klik Mulai Hitung </h3></marquee></span></center>";
                    }
                    ?>
                    </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<br>
<script>
  function cetak() {
    window.print();
  }
</script>