-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: hybriddb
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `domain`
--

DROP TABLE IF EXISTS `domain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nm_domain` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain`
--

LOCK TABLES `domain` WRITE;
/*!40000 ALTER TABLE `domain` DISABLE KEYS */;
INSERT INTO `domain` VALUES (1,'http://localhost/','Sistem Pendukung Keputusan : Pemilihan Lokasi Stand pada Batik Hatta','Y');
/*!40000 ALTER TABLE `domain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_total`
--

DROP TABLE IF EXISTS `tb_total`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_total` (
  `id_total` varchar(10) NOT NULL,
  `id_bobot` varchar(10) NOT NULL,
  `total_1` float NOT NULL,
  `total_2` float NOT NULL,
  `total_3` float NOT NULL,
  `total_4` float NOT NULL,
  `total_5` float NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_total`),
  KEY `total_id_bobot_foreign` (`id_bobot`),
  CONSTRAINT `total_id_bobot_foreign` FOREIGN KEY (`id_bobot`) REFERENCES `tbl_bobot` (`id_bobot`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_total`
--

LOCK TABLES `tb_total` WRITE;
/*!40000 ALTER TABLE `tb_total` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_total` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_alter`
--

DROP TABLE IF EXISTS `tbl_alter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_alter` (
  `id_alter` int(11) NOT NULL AUTO_INCREMENT,
  `nm_alter` varchar(15) NOT NULL,
  `no_urut` int(11) NOT NULL,
  PRIMARY KEY (`id_alter`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_alter`
--

LOCK TABLES `tbl_alter` WRITE;
/*!40000 ALTER TABLE `tbl_alter` DISABLE KEYS */;
INSERT INTO `tbl_alter` VALUES (3,'JAVA MALL',1),(35,'TRANSMART',2),(36,'DP MALL',3),(40,'CITRALAND',4),(41,'PARAGON',5);
/*!40000 ALTER TABLE `tbl_alter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_alternatif`
--

DROP TABLE IF EXISTS `tbl_alternatif`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_alternatif` (
  `id_alternatif` varchar(10) NOT NULL,
  `id_bobot` varchar(10) NOT NULL,
  `pembagi1` float DEFAULT NULL,
  `pembagi2` float DEFAULT NULL,
  `pembagi3` float DEFAULT NULL,
  `pembagi4` float DEFAULT NULL,
  `pembagi5` float DEFAULT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_alternatif`),
  KEY `alternatif_id_bobot_foreign` (`id_bobot`),
  CONSTRAINT `alternatif_id_bobot_foreign` FOREIGN KEY (`id_bobot`) REFERENCES `tbl_bobot` (`id_bobot`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_alternatif`
--

LOCK TABLES `tbl_alternatif` WRITE;
/*!40000 ALTER TABLE `tbl_alternatif` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_alternatif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_bobot`
--

DROP TABLE IF EXISTS `tbl_bobot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_bobot` (
  `id_bobot` varchar(10) NOT NULL,
  `bobot` float DEFAULT NULL,
  `bobot2` float DEFAULT NULL,
  `bobot3` float DEFAULT NULL,
  `bobot4` float DEFAULT NULL,
  `bobot5` float DEFAULT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_bobot` varchar(50) DEFAULT NULL,
  `status_hitung` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_bobot`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_bobot`
--

LOCK TABLES `tbl_bobot` WRITE;
/*!40000 ALTER TABLE `tbl_bobot` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_bobot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_kriteria`
--

DROP TABLE IF EXISTS `tbl_kriteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_kriteria` (
  `id_kriteria` int(10) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(25) NOT NULL,
  `nilai_banding` int(1) NOT NULL,
  `no_urut` int(11) NOT NULL,
  PRIMARY KEY (`id_kriteria`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_kriteria`
--

LOCK TABLES `tbl_kriteria` WRITE;
/*!40000 ALTER TABLE `tbl_kriteria` DISABLE KEYS */;
INSERT INTO `tbl_kriteria` VALUES (23,'KEAMANAN',1,1),(34,'BIAYA SEWA',1,2),(41,'LABA',1,3),(44,'LOKASI',1,4),(46,'TATA LETAK',1,5);
/*!40000 ALTER TABLE `tbl_kriteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ranking`
--

DROP TABLE IF EXISTS `tbl_ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ranking` (
  `id_ranking` varchar(10) NOT NULL,
  `id_bobot` varchar(10) NOT NULL,
  `id_alternatif` varchar(10) NOT NULL,
  `nilai_v` float DEFAULT NULL,
  `nm_alter` varchar(25) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_ranking`),
  KEY `ranking_id_bobot_foreign` (`id_bobot`),
  KEY `ranking_id_alternatif_foreign` (`id_alternatif`),
  CONSTRAINT `ranking_id_alternatif_foreign` FOREIGN KEY (`id_alternatif`) REFERENCES `tbl_alternatif` (`id_alternatif`) ON DELETE CASCADE,
  CONSTRAINT `ranking_id_bobot_foreign` FOREIGN KEY (`id_bobot`) REFERENCES `tbl_bobot` (`id_bobot`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ranking`
--

LOCK TABLES `tbl_ranking` WRITE;
/*!40000 ALTER TABLE `tbl_ranking` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ranking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nama` varchar(10) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@admin.com','21232f297a57a5a743894a0e4a801fc3','Y','1'),(2,'owner','owner@owner.com','72122ce96bfec66e2396d2e25225d70a','Y','0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-16 22:22:22
